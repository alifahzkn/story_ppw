from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import resolve
from django.apps import apps
from .views import activity, add_activity, register
from .models import Activity, Person
from .apps import Story6Config
from selenium import webdriver

# Create your tests here.

@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

        
class TestActivity(TestCase):
	def test_apakah_ada_url_activity(self):
		response = Client().get('/activity/')
		self.assertEqual(response.status_code, 200)

	def test_function_activity(self):
		found = resolve('/activity/')
		self.assertEqual(found.func, activity)

	def test_apakah_activity_menggunakan_template(self):
		response = Client().get('/activity/')
		self.assertTemplateUsed(response, 'activity.html')

	def test_apakah_form_activity_berfungsi(self):
		aktivitas = Activity.objects.create(kegiatan="abc")
		orang = Person.objects.create(kegiatan=aktivitas, nama='abc')
		response = Client().post('/activity/', data={'id':1})
		self.assertEqual(response.status_code, 302)

	def test_membuat_model_activity(self):
		kegiatan = Activity(kegiatan="abc")
		kegiatan.save()
		self.assertEqual(Activity.objects.all().count(), 1)

class TestAddActivity(TestCase):
	def test_apakah_ada_url_tambah_activity(self):
		response = Client().get('/activity/add/')
		self.assertEqual(response.status_code, 200)

	def test_function_tambah_activity(self):
		found = resolve('/activity/add/')
		self.assertEqual(found.func, add_activity)

	def test_apakah_activity_menggunakan_template(self):
		response = Client().get('/activity/add/')
		self.assertTemplateUsed(response, 'add_activity.html')

	def test_apakah_form_tambah_activity_berfungsi(self):
		response = Client().post('/activity/add/', data={'kegiatan':'abc'})
		self.assertEqual(response.status_code, 302)

class TestRegist(TestCase):
	def setUp(self):
		kegiatan = Activity(kegiatan="abc")
		kegiatan.save()

	def test_apakah_ada_url_registrasi(self):
		response = Client().post('/activity/register/1/', data={'nama':'fairuza'})
		self.assertEqual(response.status_code, 302)

	def test_apakah_registrasi_menggunakan_template(self):
		response = Client().get('/activity/register/1/')
		self.assertTemplateUsed(response, 'register.html')

class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())

