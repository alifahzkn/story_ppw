from django.db import models

# Create your models here.
class Activity(models.Model):
	kegiatan = models.CharField('Activity Name', max_length=120)
	
class Person(models.Model):
	kegiatan = models.ForeignKey('Activity', on_delete=models.CASCADE, null=True, blank=True)
	nama = models.CharField('Name', max_length=120)