from django.contrib import admin
from .models import Activity, Person

@admin.register(Activity)
class ActivityAdmin(admin.ModelAdmin):
	list_display = ('kegiatan',)

@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
	list_display = ('nama', 'kegiatan')