from django.urls import path
from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.activity, name='activity'),
    path('add/', views.add_activity, name='add_activity'),
    path('register/<int:index>/', views.register, name='register'),
]