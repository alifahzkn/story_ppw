from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import resolve
from django.apps import apps
from .views import *
from .models import *
from .apps import Story5Config
from selenium import webdriver

# Create your tests here.
class TestSubject(TestCase):
	def test_apakah_ada_url_subject(self):
		response = Client().get('/subject/')
		self.assertEqual(response.status_code, 200)

	def test_fungsi_subject(self):
		found = resolve('/subject/')
		self.assertEqual(found.func, subject)

	def test_apakah_subject_menggunakan_template(self):
		response = Client().get('/subject/')
		self.assertTemplateUsed(response, 'subject_list.html')