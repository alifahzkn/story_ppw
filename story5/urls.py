from django.urls import path
from . import views

app_name = 'story5'

urlpatterns = [
    path('', views.subject, name='subject'),
    path('detail/<int:index>/', views.subject_detail, name='subject_detail'),
    path('add/', views.add_subject, name='add_subject'),
]