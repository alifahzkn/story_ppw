from django.urls import path
from . import views

app_name = 'story4'

urlpatterns = [
    path('', views.home, name='home'),
    path('time/', views.time, name='time'),
    path('time/<str:index>/', views.time, name='time'),
]