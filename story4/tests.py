from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import resolve
from django.apps import apps
from .views import *
from .models import *
from .apps import Story4Config
from selenium import webdriver

# Create your tests here.
class TestHome(TestCase):
	def test_apakah_url_kosong_mengembalikan_homepage(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_fungsi_home(self):
		found = resolve('/')
		self.assertEqual(found.func, home)

	def test_apakah_home_menggunakan_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'homepage.html')